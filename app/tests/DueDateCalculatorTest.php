<?php

declare(strict_types=1);

namespace Emarsys\Homework\Tests;

use Emarsys\Homework\DueDateCalculator;
use PHPUnit\Framework\TestCase;

class DueDateCalculatorTest extends TestCase
{
    /**
     * @var DueDateCalculator
     */
    private $dueDateCalculator;


    protected function setUp(): void
    {
        $this->dueDateCalculator = new DueDateCalculator();
    }

    public function provider(): array
    {
        return [
            ['2017-06-27 10:00:00', '2017-06-26 16:00:00', 2],
            ['2017-06-27 10:00:00', '2017-06-27 09:00:00', 1],
            ['2017-06-30 10:00:00', '2017-06-28 16:00:00', 10],
            ['2017-06-14 13:00:00', '2017-06-06 16:00:00', 45],
            ['2017-06-26 09:01:01', '2017-06-23 16:01:01', 1]
        ];
    }

    /**
     * @dataProvider provider
     */
    public function testCalculateDueDate(string $expectedDate, string $submitDate, int $turnaroundTime): void
    {
        $dueDate = $this->dueDateCalculator->calculateDueDate(new \DateTime($submitDate), $turnaroundTime);
        $this->assertEquals(new \DateTime($expectedDate), $dueDate);
    }

    /**
     * @expectedException \Emarsys\Homework\OutOfRangeDateRangeException
     * @expectedExceptionMessage You can only report bugs in working hours!
     */
    public function testInvalidSubmitHourException(): void
    {
        $this->dueDateCalculator->calculateDueDate(new \DateTime('2017-06-27 17:00:00'), 1);
    }

    /**
     * @expectedException \Emarsys\Homework\OutOfRangeDateRangeException
     * @expectedExceptionMessage Invalid -25 turnaround time
     */
    public function testInvalidTurnaroundException(): void
    {
        $this->dueDateCalculator->calculateDueDate(new \DateTime('2017-06-27 09:00:00'), -25);
    }
}
