<?php

declare(strict_types=1);

namespace Emarsys\Homework;

class DueDateCalculator
{
    const WORK_START_DATE = 9;

    const WORK_END_DATE = 17;

    const WORKING_HOUR = self::WORK_END_DATE - self::WORK_START_DATE;

    public function calculateDueDate(\DateTime $submitDate, int $turnaroundTime): \DateTime
    {
        $this->validateParameters($submitDate, $turnaroundTime);

        return $this->calculateDate($submitDate, $turnaroundTime);
    }

    private function calculateDate(\DateTime $dueDate, int $turnaroundTime): \DateTime
    {
        $numberOfFullDays = floor($turnaroundTime / self::WORKING_HOUR);
        $this->addWeekdays($dueDate, (int)$numberOfFullDays);
        $estimateHours = (int)$dueDate->format('H') + ($turnaroundTime % self::WORKING_HOUR);
        $restHours = $estimateHours;
        if ($estimateHours >= self::WORK_END_DATE) {
            $this->addWeekdays($dueDate, 1);
            $restHours = self::WORKING_HOUR + ($estimateHours % self::WORKING_HOUR);
        }

        $this->setTime($dueDate, (int)$restHours);

        return $dueDate;
    }

    private function validateParameters(\DateTime $submitDate, int $turnaroundTime): void
    {
        if (!$this->isValidSubmitDate($submitDate) || $this->isWeekend($submitDate)){
            throw new OutOfRangeDateRangeException(sprintf('You can only report bugs in working hours!'));
        }
        if ($turnaroundTime <= 0) {
            throw new OutOfRangeDateRangeException(sprintf('Invalid %s turnaround time', $turnaroundTime));
        }
    }

    private function setTime(\DateTime $dueDate, int $estimateHours): void
    {
        $dueDate->setTime($estimateHours, (int)$dueDate->format('i'), (int)$dueDate->format('s'));
    }

    private function addWeekdays(\DateTime $dueDate, int $days): void
    {
        $dueDate->add(\DateInterval::createFromDateString(sprintf('%s weekday', $days)));
    }

    private function isValidSubmitDate(\DateTime $submitDate): bool
    {
        $hour = (int)$submitDate->format('H');

        return $hour >= self::WORK_START_DATE && $hour < self::WORK_END_DATE;
    }

    private function isWeekend(\DateTime $dueDate): bool
    {
        return $dueDate->format('N') >= 6;
    }
}
